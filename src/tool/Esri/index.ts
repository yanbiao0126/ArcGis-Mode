import { loadModules, setDefaultOptions } from 'esri-loader'
import config from '../../config'

export default class Esri {
    private Map: any;
    private View: any;
    constructor() {
        // this.init()
    }
    async init(dom) {
        let self = this
        setDefaultOptions(config.Esri.base)
        let [Map, MapView] = await loadModules(['esri/Map', 'esri/views/MapView'])
        self.Map = new Map({
            basemap: 'topo-vector'
        })
        self.View = new MapView({
            container: dom,
            map: self.Map,
            zoom: 15
        })
    }
}